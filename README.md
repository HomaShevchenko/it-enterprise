# it-enterprise

## Project setup

### Install dependencies

```
npm install
```

### Runs dev-server
```
npm start
```

### Compiles production bundle and runs server
```
npm run prod
```

### Lints and fixes files
```
npm run lint
```
