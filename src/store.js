import Vue from 'vue'
import Vuex from 'vuex'
import { fetchCourses } from './api'

Vue.use(Vuex)

const getOptions = (entries, prop) =>
  entries.reduce((options, entry) => [
    ...options,
    ...entry[prop].filter((option) =>
      !options.find((item) =>
        item.code === option.code
      )),
  ], [])

const filterByPropValues = (entries, prop, values) =>
  entries.reduce((options, entry) => [
    ...options,
    ...entry[prop].find((option) =>
      values.includes(option.code)) ?
    [entry] :
    []
  ], [])

const state = {
  isLoaded: false,
  courses: [],
  tagsOptions: [],
  tagsValues: [],
  rolesOptions: [],
  rolesValues: [],
  levelsOptions: [],
  levelsValues: [],
  productsOptions: [],
  productsValues: [],
}

const mutations = {
  SET_IS_LOADED(state, payload) {
    state.isLoaded = payload
  },
  SET_COURSES(state, payload) {
    state.courses = payload
  },
  SET_FILTER_VALUES(state, payload) {
    state[`${payload.name}Values`] = payload.values
  },
  SET_FILTER_OPTIONS(state, payload) {
    state[`${payload.name}Options`] = payload.values
  },
}

const actions = {
  async loadCourses(context) {
    context.commit('SET_IS_LOADED', false)
    const courses = await fetchCourses()
    context.commit('SET_COURSES', courses)
    context.commit('SET_FILTER_OPTIONS', {
      name: 'tags',
      values: getOptions(state.courses, 'tags'),
    })
    context.commit('SET_FILTER_OPTIONS', {
      name: 'roles',
      values: getOptions(state.courses, 'roles'),
    })
    context.commit('SET_FILTER_OPTIONS', {
      name: 'levels',
      values: getOptions(state.courses, 'levels'),
    })
    context.commit('SET_FILTER_OPTIONS', {
      name: 'products',
      values: getOptions(state.courses, 'products'),
    })
    context.commit('SET_IS_LOADED', true)
  },
  async filterByTags(context, values) {
    context.commit('SET_IS_LOADED', false)
    const courses = await fetchCourses()
    const nextCourses = (!values.length) ?
      courses :
      filterByPropValues(courses, 'tags', values)
    context.commit('SET_COURSES', nextCourses)
    context.commit('SET_FILTER_VALUES', { name: 'tags', values })
    context.commit('SET_IS_LOADED', true)
  },
  async filterByRoles(context, values) {
    context.commit('SET_IS_LOADED', false)
    const courses = await fetchCourses()
    const nextCourses = (!values.length) ?
      courses :
      filterByPropValues(courses, 'roles', values)
    context.commit('SET_COURSES', nextCourses)
    context.commit('SET_FILTER_VALUES', { name: 'roles', values })
    context.commit('SET_IS_LOADED', true)
  },
  async filterByLevels(context, values) {
    context.commit('SET_IS_LOADED', false)
    const courses = await fetchCourses()
    const nextCourses = (!values.length) ?
      courses :
      filterByPropValues(courses, 'levels', values)
    context.commit('SET_COURSES', nextCourses)
    context.commit('SET_FILTER_VALUES', { name: 'levels', values })
    context.commit('SET_IS_LOADED', true)
  },
  async filterByProducts(context, values) {
    context.commit('SET_IS_LOADED', false)
    const courses = await fetchCourses()
    const nextCourses = (!values.length) ?
      courses :
      filterByPropValues(courses, 'products', values)
    context.commit('SET_COURSES', nextCourses)
    context.commit('SET_FILTER_VALUES', { name: 'products', values })
    context.commit('SET_IS_LOADED', true)
  },
}

export default new Vuex.Store({
  state,
  mutations,
  actions,
})
