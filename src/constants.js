export const TAGS = 'tags'
export const TAGS_RU = 'Тэги'

export const DIFFICULITY = 'difficulity'
export const DIFFICULITY_RU = 'Уровень сложности'

export const ROLES = 'roles'
export const ROLES_RU = 'Роли'

export const PRODUCTS = 'products'
export const PRODUCTS_RU = 'Продукты'
