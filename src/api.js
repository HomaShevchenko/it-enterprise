import axios from 'axios';

export const fetchCourses = () =>
  axios
    .get('/api/courses')
    .then((res) => JSON.parse(JSON.stringify(res.data)))
    .catch((err) => { throw new Error(err) });
