import Vue from 'vue'
import { Row, Col, Card, Option, Button, Select, Tag } from 'element-ui'
import store from './store'
import App from './components/app.vue'

import 'element-ui/packages/theme-chalk/src/index.scss'

Vue.config.productionTip = false

Vue.component(Row.name, Row);
Vue.component(Col.name, Col);
Vue.component(Card.name, Card);
Vue.component(Option.name, Option);
Vue.component(Button.name, Button);
Vue.component(Select.name, Select);
Vue.component(Tag.name, Tag);

new Vue({
  store,
  el: '#app',
  render: h => h(App)
})
