const axios = require('axios')
const http = require('http')
const morgan = require('morgan')
const express = require('express')
const bodyParser = require('body-parser')

const fetchCourses = async () => {
  const url = 'http://m.it.ua/ws/WebService.asmx/ExecuteEx?calcId=_MOCKJS.GET_COURSES&args=null&ticket='
  const { data } = await axios.post(url, {}, {
    withCredentials: true,
    headers: {
      "Content-Type": "application/json"
    }
  })
  try {
    const { courses } = JSON.parse(data
      .replace(/\\/gm, '')
      .replace(/amp\;/gm, '')
      .replace('<?xml version="1.0" encoding="utf-8"?>\r\n<string xmlns="http://tempuri.org/">"', '')
      .replace('"</string>', ''))
    return courses
  } catch(e) {
    console.error(e)
    throw 'Error while parsing response'
  }
}

const coursesMiddleware = async (req, res) => {
  try {
    const courses = await fetchCourses()
    res.status(200).json(courses)
  } catch (e) {
    res.status(500).json('Error while fetching courses', e)
  }
}

const app = express()
app.set('trust proxy', true)
app.use(morgan(':method :url :status :res[content-length] - :response-time ms'))
app.use(bodyParser.json())
if (process.env.NODE_ENV === 'production') {
  app.use(express.static('dist'))
}
app.get('/api/courses', coursesMiddleware)
app.listen(3000, () => console.log(`Express started http://localhost:3000/`))
