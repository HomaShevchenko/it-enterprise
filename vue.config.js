module.exports = {
  devServer: {
    hot: true,
    port: 8080,
    host: 'localhost',
    disableHostCheck: true,
    proxy: {
      '^https\:\/\/m\.it\.ua': {
        target: 'https://m.it.ua',
        withCredentials: true,
        // protocol: 'http:',
        // port:3000,
        // ws: true,
      },
      '^/api': {
        withCredentials: true,
        target: 'http://localhost:3000',
        // protocol: 'http:',
        port:3000,
        // ws: true,
      },
    },
  },
  parallel: undefined,
  publicPath: undefined,
  outputDir: undefined,
  assetsDir: undefined,
  lintOnSave: undefined,
  productionSourceMap: undefined,
  runtimeCompiler: true,
}
